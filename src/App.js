import './App.css'
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import UserModal from './Components/FormEdit'; 
import { Table, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  const [users, setUsers] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);

  const handleCloseModal = () => setShowModal(false);
  const handleShowModal = () => setShowModal(true);

  const handleCreateUser = async (userData) => {
    try {
      await axios.post('https://63fdd632cd13ced3d7bfe0e7.mockapi.io/users', userData);
      handleCloseModal();
      fetchUsers();
    } catch (error) {
      console.error(error);
    }
  };

  const handleEditUser = async (userData) => {
    try {
      await axios.put(`https://63fdd632cd13ced3d7bfe0e7.mockapi.io/users/${selectedUser.id}`, userData);
      handleCloseModal();
      fetchUsers();
    } catch (error) {
      console.error(error);
    }
  };

  const handleDeleteUser = async (userId) => {
    const confirmed = window.confirm("bạn có chắc là muốn xoá không ?")
    if(confirmed){
      try {
        await axios.delete(`https://63fdd632cd13ced3d7bfe0e7.mockapi.io/users/${userId}`);
        fetchUsers();
      } catch (error) {
        console.error(error);
      }
      
    }
  };

  const handleEditButtonClick = (user) => {
    setSelectedUser(user);
    handleShowModal();
  };

  const handleCreateButtonClick = () => {
    setSelectedUser(null);
    handleShowModal();
  };

  const fetchUsers = async () => {
    try {
      const response = await axios.get('https://63fdd632cd13ced3d7bfe0e7.mockapi.io/users');
      setUsers(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchUsers();
  }, []);
    
  

  
  return (
    <div>
      <nav>
      <h3>Home</h3>
    </nav>
    
    <h1> Quản lý sinh viên </h1>
    
      <button id='btn' className='btn-add' variant="primary" onClick={handleCreateButtonClick}> Thêm sinh viên</button>
      
      <table striped bordered hover>
        <thead>
          <tr>
            <th>STT</th>
            <th>ID</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={user.id}>
              <td>{index + 1}</td>
              <td>{user.id}</td>
              <td>{user.name}</td>
              <td>{user.phone}</td>
              <td>{user.address}</td>
              <td>
                <button id='btn' className='btn-edit' variant="primary" onClick={() => handleEditButtonClick(user)}> Edit </button> 
                <button variant="danger" onClick={() => handleDeleteUser(user.id)}id='btn' className='btn-delete'>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      
      <UserModal show={showModal} handleClose={handleCloseModal} handleSubmit={selectedUser ? handleEditUser : handleCreateUser} user={selectedUser} />
  
       
    </div>
    
  );
}

export default App;

