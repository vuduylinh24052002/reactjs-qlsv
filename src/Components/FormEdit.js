
import React, { useState, useEffect } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

const UserModal = ({ show, handleClose, handleSubmit, user },props) => {
  const [userData, setUserData] = useState({
    name: '',
    phone: '',
    address: '',
  });

  useEffect(() => {
    if (user) {
      setUserData({
        name: user.name,
        phone: user.phone,
        address: user.address,
      });
    } else {
      setUserData({
        name: '',
        phone: '',
        address: '',
      });
    }
  }, [user]);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setUserData((prevUserData) => ({ ...prevUserData, [name]: value }));
  };

  const handleSubmitButtonClick = () => {
    handleSubmit(userData);
  };
  return (
    <Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>{user ? 'Edit User' : 'Create User'}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form >
          <Form.Group controlId="formName">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" placeholder="Enter name" name="name" value={userData.name} onChange={handleChange} />
          </Form.Group>
          <Form.Group controlId="formPhone">
            <Form.Label>Phone</Form.Label>
            <Form.Control type="text" placeholder="Enter phone" name="phone" value={userData.phone} onChange={handleChange} />
          </Form.Group>
          <Form.Group controlId="formAddress">
            <Form.Label>Address</Form.Label>
            <Form.Control type="text" placeholder="Enter address" name="address" value={userData.address} onChange={handleChange} />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" style={{backgroundColor :"#f44336"}}  onClick={handleClose}>
          Close
        </Button>
        <Button variant="primary" onClick={handleSubmitButtonClick}>
          {user ? 'Save Changes' : 'Create'}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default UserModal;
